Get Better Project Goals:

Create games with fun goals to help you:
  - explore
  - get better at things

Create proof requirements:
  - Photo of blah
  - Video of blah
  - web link
  - text
